package com.banco;

public class Cliente {
    private double saldo;
    private boolean ocupado;

    Cliente(){
        this.setTodosDatos();
    }

    Cliente( double saldo){
        this.setTodosDatos( saldo);
    }

    Cliente(Cliente cliente){
        this.setTodosDatos(cliente);
    }

    private void setTodosDatos(){
        this.saldo = 0.0;
        this.ocupado = false;
    }

    public void setTodosDatos( double saldo){
        this.validarSaldo(saldo);
        this.setOcupado();
    }

    public void setTodosDatos(Cliente cliente){
        this.validarSaldo(cliente.getSaldo());
        this.setOcupado();
    }

    public Cliente getTodosDatos(){
        temporal.saldo = this.getSaldo();
        temporal.ocupado = this.isOcupado();
        return temporal;
    }

   
    private void validarSaldo(double saldo){
        if(saldo <= 0.0){
            saldo = 0.0;
        }
        this.saldo = saldo;
    }

    private boolean tieneSaldo(){
        return this.saldo > 0.0;
    }

    private boolean tieneDatos(){
        return !(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ');
        /*if(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ')
            return false;
        else
            return true;*/
    }

    private void setOcupado() {
        if(!this.tieneDatos()){
            this.setTodosDatos();
        }
        else{
            this.ocupado = true;
        }
    }

    

    public double getSaldo() {
        return saldo;
    }

    public boolean isOcupado() {
        return ocupado;
    }

    public void eliminar(){
        this.setTodosDatos();
    }

    public void abonarSaldo(double cuanto){
        if(this.tieneDatos()) {
            if(cuanto > 0.0){
                this.saldo += cuanto;
            }
            else{
                System.out.println("\tImposible, no puedes abonar saldos negativos o nada.");
            }
        }
    }

    public void retirarSaldo(double cuanto){
        if(this.tieneDatos()){
            if(cuanto > 0.0){
                if(this.tieneSaldo()){
                    if(this.saldo >= cuanto)
                        this.saldo -= cuanto;
                    else
                        System.out.println("\tImposible, tú saldo es insuficiente para retirar " + cuanto + ".");
                }
                else
                    System.out.println("\tImposible, no hay saldo.");
            }
            else
                System.out.println("\tImposible, no puedes retirar saldos negativos o nada.");
        }
    }

    @Override
    public String toString(){
        return
                "| -> Saldo: |" + this.saldo +
                "| -> Ocupado: |" + this.ocupado + "|";
    }

    public void imprimirCliente(){
        System.out.println("\t\t" + this.toString());
    }
}
