package com.banco;

public class Persona {
    private String nombre;
    private String apellidop;
    private String apellidom;
    private char sexo;
    private String curp;
    private String direccion;
    private int telefono;

    Persona(){
        this.setTodosDatos();
    }

    Persona(String nombre, String apellidop, String apellidom, char sexo, String curp,String direccion, int telefono){
        this.setTodosDatos(nombre, sexo, saldo);
    }

    Persona(Persona persona){
        this.setTodosDatos(persona);
    }

    private void setTodosDatos(){
        this.nombre = null;
        this.apellidop = null;
        this.apellidom = null;
        this.sexo = ' ';
        this.curp = null;
        this.direccion = null;
        this.telefono = 0;
        
    }

    public void setTodosDatos(String nombre, char sexo, double saldo){
        this.nombre = nombre;
        this.apellidop = apellidop;
        this.apellidom = apellidom;
        this.validarsexo(sexo);
        this.curp = curp;
        this.direccion = direccion;
        this.telefono = 0;
    }

    public void setTodosDatos(Persona persona){
        this.nombre = persona.getnombre();
        this.apellidop = persona.getApellidop();
        this.apellidom = persona.getApellidom();
        this.validarsexo(persona.getSexo());
        this.curp = persona.getcurp();
        this.direccion = persona.getDireccion();
        this.telefono = persona.getTelefono;
    }

    public Persona getTodosDatos(){
        Persona temporal = new Persona();
        temporal.nombre = this.getNombre();
        temporal.apellidop = this.getApellidop();
        temporal.apellidom = this.getApellidom();
        temporal.sexo = this.getSexo();
        temporal.curp = this.getcurp();
        temporal.direccion = this.getDireccion;
        temporal.telefono = this.getTelefono;

        return temporal;
    }
 private void validarSexo(char sexo){
        if(sexo != 'M' && sexo != 'F'){
            sexo = ' ';
        }
        this.sexo = sexo;
    }
private boolean tieneDatos(){
        return !(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ');
        /*if(this.nombre == null || this.nombre.equalsIgnoreCase(" ") || this.sexo == ' ')
            return false;
        else
            return true;*/
    }

    public String getNombre(){
        return this.nombre;
    }
    public String getApellidop(){
        return this.appelidop;
    }
    public String getApellidom(){
        return this.apellidom;
    }
    public char getSexo() {
        return sexo;
    }
    public String curp(){
        return this.curp;
    }
    public String direccion(){
        return this.direccion;
    }
    public int telefono(){
        return telefono;
    }

public void eliminar(){
        this.setTodosDatos();
    }

    public void cambiarNombre(String nuevoNombre){
        if(nuevoNombre != null && !nuevoNombre.equalsIgnoreCase(" ")){
            this.nombre = nuevoNombre;
        }
        this.setOcupado();
    }

    public void cambiarSexo(char sexo){
        if(sexo == 'M' || sexo == 'F'){
            this.sexo = sexo;
        }
        this.setOcupado();
    }
   @Override
    public String toString(){
        return "-> Nombre: |" + this.nombre +
                "| -> Apellido Paterno: |" + this.apellidop +
                "| -> Apellido Materno: |" + this.apellidom +
                "| -> Sexo: |" + this.sexo +
                "| -> Curp: |" + this.curp +
                "| -> Direccion: |" + this.direccion +
                "| -> Telefono: |" + this.telefono;
    }

    public void imprimirCliente(){
        System.out.println("\t\t" + this.toString());
    }
}
